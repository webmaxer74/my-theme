const gulp = require('gulp');
const parcel = require('gulp-parcel');
const cssmin = require('gulp-cssnano');
const prefix = require('gulp-autoprefixer');
const copyAssets = require('gulp-css-copy-assets').default;
const sass = require('gulp-sass');

sass.compiler = require('node-sass');

const themeKit = require('@shopify/themekit');
const mode = require('gulp-mode')({modes: ["production", "development", "staging"]});
const env = mode.production() ? 'production' : mode.staging() ? 'staging' : 'development';


function js() {
  return gulp.src('./frontend/js/theme.js', {read:false})
    .pipe(parcel())
    .pipe(gulp.dest('./shopify/assets'));
}

function css() {
  return gulp.src(['./frontend/styles/**/*', '!./frontend/styles/vendor/**/*'])
    .pipe(sass({ includePaths: ['node_modules'] }).on('error', sass.logError))
    .pipe(prefix({
      overrideBrowserslist: ['last 2 versions']
    }))
    .pipe(mode.production(cssmin()))
    .pipe(copyAssets())
    .pipe(gulp.dest('./shopify/assets'))
}


function watchFiles() {
  gulp.watch('./frontend/js/**/*.js', gulp.series(js));
  gulp.watch('./frontend/styles/**/*', gulp.series(css));

  themeKit.command('watch', {
    env: env,
    allowLive: true
  })
}

function deployFiles() {
  themeKit.command('deploy', {
    env: env
  });
}

function syncFiles() {
  themeKit.command('download', {
    env: env
  });
}

const watch = gulp.series(watchFiles);
const build = gulp.series(js, css);
const deploy = gulp.series(js, css, deployFiles);
const sync = gulp.series(syncFiles);

exports.watch = watch;
exports.build = build;
exports.deploy = deploy;
exports.sync = sync;
exports.default = build;