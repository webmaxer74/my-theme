.widget {
  margin-top: 16px;
  width: 370px;
  position: absolute;
  right: 16px;
  top: 100%;
  background: #fff;
  z-index: 2;
  border: 1px solid #ccc;
  border-top: 0px;

  &:before {
    content: "";
    position: absolute;
    right: 0;
    bottom: 100%;
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 0 10px 10px 10px;
    border-color: transparent transparent #000 transparent;
  }
}

.minicart {
  opacity: 1;
  z-index: 99;
  right: auto;
  top: 30px;
  transform: translate(0, 0);
  transition: 0.5s opacity ease-out, 0.5s transform ease-out;
  &.hide {
    display: flex !important;
    opacity: 0;
    z-index: -1;
    transform: translate(0, -200%);
  }
}

.minicart__header {
  padding: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-bottom: 1px solid #000;
  border-top: 3px solid #000;
  text-transform: uppercase;
  font-size: 15px;
  font-weight: 900;
  text-align: center;
}

.minicart__body,
.minicart__footer  {
  padding: 0 20px;
  text-transform: initial;
}


.minicart__btn {
  @extend .btn;
  width: auto;
  min-width: 47%;
  &.btn-white {
    @extend .btn-border;
  }
  &.m-checkout {
    @extend .btn-primary;
    &:hover {
      background: #000;
      color: #fff;
    }
  }
}

.minicart__items {
  max-height: 420px;
  overflow: hidden;
  overflow-y: auto;
}

.minicart__item {
  padding: 20px 0 10px 0;
  display: flex;
  align-items: flex-start;
  border-bottom: 1px solid #dfdfdf;
  position: relative;

  .price {
    font-weight: bold;
  }
}

.minicart__item-image {
  display: block;
  width: 90px;
}

.minicart__item-info {
  flex: 1;
  padding-left: 20px;
  font-size: 0.88rem;
}

.minicart__item-vendor {
  margin: 0 0 5px 0;
}

.minicart__item-title {
  margin: 0;
  font-size: 1rem;
  font-weight: 400;
  color: #000;
  a {
    color: inherit;
    text-decoration: none;
  }
}

.minicart__item-price {
  margin: 15px 0;
  display: flex;
  align-items: center;
  justify-content: space-between;
}

.minicart__subtotal {
  padding: 20px 0;
  font-size: 15px;
  text-align: center;
  border-top: 1px solid #dfdfdf;

  .price {
    margin-left: 10px;
    font-weight: bold;
    font-size: 1rem;
  }
}



.minicart__item-options {
  margin-top: 0;
  color: #888;
}

.minicart__buttons {
  padding-bottom: 20px;
}

.minicart__close {
  margin-bottom: 20px;
  text-decoration: underline;
  background: none;
  border: none;
  font-family: $font-primary;
  &:active,
  &:focus {
    outline: none;
  }
}

@include media-query('small') {
  .widget {
    width: auto;
    position: fixed;
    top: 64px;
    right: auto;
    left: 0;

    &:before {
      display: none;
    }
  }
  .minicart {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    width: 100%;
    left: 0;
    right: auto;
    bottom: auto;
    &.is-full {
      bottom: 0;
    }
  }
  .minicart__body {
    margin-bottom: auto;
    overflow: auto;
  }
  .minicart__items {
    max-height: none;
    overflow: visible;
  }
}