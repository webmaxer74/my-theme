import $ from 'jquery';

export default function(elem, className = 'hide', secondElem) {
  $(document).mouseup(function (e) {
    var container = $(elem);
    var second = $(secondElem);

    if (second) {
      if (!container.is(e.target) && !second.is(e.target) && container.has(e.target).length === 0 && second.has(e.target).length === 0) {
        if (className == 'is-open') {
          container.removeClass(className);
        } else {
          container.addClass(className);
        }
      }
    } else {
      if (!container.is(e.target) && container.has(e.target).length === 0) {
        if (className == 'is-open') {
          container.removeClass(className);
        } else {
          container.addClass(className);
        }
      }
    }
  });
}