export default function ({fn, path, args}) {
  try {
    fn(args);
    if (args && args.resize) {
      $(window).resize(function() {
        fn(args);
      })
    }
  } catch (e) {
    console.error(path, e);
  }
}