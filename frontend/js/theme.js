import register from './tools/register';
import './jq';
import header from './modules/header';
import slider, { mountSlider } from './modules/slider';
import qty from './modules/qty-selector';
import product from './modules/product';
import minicart from './modules/minicart';
import login from './modules/login';
import customerAddresses from './modules/account';
import animation from './modules/animation';
import cartPage from './modules/cartPage';
import popup from './modules/popup';
import video from './modules/video';
import productVideo from './modules/product-video';
import subscriptionPopup from './modules/subscriptionPopup';
import collectionFilter from './modules/collectionFilter';
import searchDropdown from './modules/searchDropdown';
import currency from './modules/currency-switcher';
import geolocation from './modules/geolocation';
import tabs from './modules/tabs';
import accordions from './modules/accordions';
import faq from './modules/faq';
import stickyFooter from './modules/stickyFooter';
import floatingLabels from './modules/floatingLabels';


$(document).ready(() => {
  register({ fn: header, path: './modules/header' });
  register({ fn: stickyFooter, path: './modules/stickyFooter' });
  register({ fn: slider, path: './modules/slider' });
  register({ fn: product, path: './modules/product' });
  register({ fn: qty, path: './modules/qty' });
  register({ fn: login, path: './modules/login' });
  register({ fn: customerAddresses, path: './modules/customerAddresses' });
  register({ fn: animation, path: './modules/animation' });
  register({ fn: cartPage, path: './modules/cartPage' });
  register({ fn: popup, path: './modules/popup' });
  register({ fn: themeEditor, path: './modules/themeEditor' });
  register({ fn: tabs, path: './modules/tabs' });
  register({ fn: accordions, path: './modules/accordions' });
  register({ fn: minicart, path: './modules/minicart' });
  register({ fn: productVideo, path: './modules/productVideo' });
  register({ fn: subscriptionPopup, path: './modules/subscriptionPopup' });
  register({ fn: floatingLabels, path: './modules/floatingLabels' });
  register({ fn: video, path: './modules/video' });
  register({ fn: collectionFilter, path: './modules/collectionFilter' });
  register({ fn: searchDropdown, path: './modules/searchDropdown' });
  register({ fn: currency, path: './modules/currency' });
  register({ fn: geolocation, path: './modules/geolocation' });
  register({ fn: faq, path: './modules/faq' });
});

const themeEditor = function() {
  if (Shopify.designMode) {
    $('body').removeClass('animations-on');
  }

  function refreshScripts(e) {

    // Refresh slick
    const $slider = $(e.target).find('[data-hor-slider]');
    if ($slider.length && !$slider.hasClass('slick-initialized')) {
      mountSlider($slider);
    }

    // Refresh lookbook
    const $lookbook = $(e.target).find('.lookbook-image-gallery');
    if ($lookbook.length) {
      lookbook();
    }

    // Refresh AOS
    if (AOS) {
      AOS.refreshHard();
    }

  }
  
  $(document).on('shopify:section:select shopify:section:load shopify:section:reorder', refreshScripts);
}