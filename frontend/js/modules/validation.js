import $ from 'jquery';

const $input = $('[data-validation-input]');
const $btn = $('[data-validation-btn]');
const $error = $('[data-validation-error]');
const $success = $('[data-validation-success]');

export default function() {
  $btn.on('click', function (e) { 
    e.preventDefault();
    const code = $input.val();
    if (window.validationCodes.includes(code)) {
      $error.hide();
      $success.show();
    } else {
      $error.show();
      $success.hide();
    }
  });
}