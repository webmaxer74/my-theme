import $ from 'jquery';

const $search = $('[data-search]');

const init = function () {
  $('[data-search-toggle]').on('click', function(e) {
    e.preventDefault();
    $search.fadeIn(100);
  });
  $('[data-close-search]').on('click', function(e) {
    e.preventDefault();
    $search.fadeOut(100);
  });
}

export default init;