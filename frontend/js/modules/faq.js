export default function() {
  const $toggle = $('[data-faq-section-toggle]');
  const $section = $('[data-faq-section]');
  if ($toggle.length) {
    $toggle.click(function (e) { 
      const currentSection = $(this).data('faq-section-toggle');
      $toggle.removeClass('is-active');
      $(this).addClass('is-active');
      $section.removeClass('is-active');
      $section.filter(`[data-faq-section=${currentSection}]`).addClass('is-active');
    });
  }
}