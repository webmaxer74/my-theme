import $ from 'jquery';
import select2 from 'select2';
export default function() {
  select2($);


  // Select with icon:
  var formatState = function(data, container) {
    if (data.element) {
      $(container).addClass($(data.element).attr("class"));
    }
    return data.text;
  }

  $("[data-custom-select]").select2({
    templateSelection: formatState,
    templateResult: formatState,
    minimumResultsForSearch: -1
  });

  var selectors = {
    multicurrencySelector: '[data-currency-selector]',
  };
  $(selectors.multicurrencySelector).on('change', function () {
    $(this)
      .parents('form')
      .submit();
  });
}