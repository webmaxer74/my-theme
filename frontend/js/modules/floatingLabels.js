export default function() {
  $('[data-floating-label]').on('focusin', function() {
    $(this).parent().addClass('is-focused')
  }).on('focusout', function() {
    if ($(this).val() == "") {
      $(this).parent().removeClass('is-focused')
    }
  })
}
