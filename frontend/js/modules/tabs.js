export default function() {
  $('[data-tab]').click(function (e) { 
    e.preventDefault();
    const index = $(this).data('tab');
    const $parent = $(this).closest('[data-tabs]');
    $('[data-tab], [data-tab-content]', $parent).removeClass('is-active');
    $(this).addClass('is-active');
    $(`[data-tab-content=${index}]`, $parent).addClass('is-active');
  });
  return 'tabs';
}