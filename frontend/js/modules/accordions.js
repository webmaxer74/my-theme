export default function() {
  $(document).on('click', '[data-accordion-button]', function (event) {
    const $container = $(this).closest('[data-accordions-container]');

    if ($container.data('mobile-only') == "1" && $(window).outerWidth() > 989 ) {
      return false;
    }

    const $currentBtn = $(this);
    const $currentContent = $(this).siblings('[data-accordion-content]');
    $currentBtn.toggleClass('is-open');
    $currentContent.slideToggle();
    $('[data-accordion-button]', $container).not($currentBtn).removeClass('is-open');
    $('[data-accordion-content]', $container).not($currentContent).slideUp();
  });
}