import $ from 'jquery';
import CartJS from '/vendor/cart.min.js';
import tinybind from 'tinybind';
import { formatMoney } from '/tools/moneyFormats';
import PerfectScrollbar from 'perfect-scrollbar';
import 'regenerator-runtime/runtime'

let data = {
  cart: {},
  update(newCart) {
    this.cart = newCart
  }
};
let open = false;
const $minicart = $('[data-minicart]');
const scrollBar = new PerfectScrollbar('.minicart__items');


async function addToCart(id, qty, properties) {
  CartJS.addItem(id, qty, properties, {
    success: function (data, textStatus, jqXHR) {
      open = true;
    },
    error: function (x) {
      console.error("minicart.js: ajax error on adding item to cart", x)
    }
  });
}

const getProperties = function() {
  let properties = []
  $('[data-product-property]').each(function (index, element) {
    let key = $(element).data('product-property');
    let value = $(element).val();
    if (key && key != 'hidden' && value && !$(element).is(':checkbox')) {
      properties[key] = value;
    }
    if ($(element).is(':checkbox:checked')) {
      properties[key] = value;
    }
  });
  return properties;
}

async function addToCartHandler () {
  const variantId = $('[data-all-variants]').val();
  const qty = 1;
  let properties = getProperties();
  if (variantId) {
    await addToCart(variantId, qty, properties);
  } else if ($('[data-all-variants] option:selected').data('multi-variants')) {
    let $variants = $('[data-all-variants] option:selected').data('multi-variants');
    $variants.forEach(function(id) {
      addToCart(id, qty, properties);
    });
  } else {
    console.error("minicart.js: can't add item to cart, wrong variantId", variantId);
  }
}


const removeFromCart = function () {
  if ($(this).data('line') != undefined) {
    let id = $(this).data('line');
    if (id != undefined) {
      CartJS.removeItem(id + 1);
    } else {
      console.error("Can't remove item", id, " from the cart!");
    }
  } else {
    console.error("Can't remove item to the cart since data-line is not present in the remove-from-cart button!");
  }
}


const updateMinicart = function() {
  $.get('/cart?view=json', function(result) {
    const newCart = JSON.parse(result);
    const items = newCart.items.filter(item => item.product_handle != 'monogram');
    const monogramItem = newCart.items.find(item => item.product_handle == 'monogram');
    if (monogramItem) items.push(monogramItem);
    newCart.items = items;
    data.update(newCart);
    scrollBar.update();
    if (open) {
      $minicart.addClass('is-open');
      open = false;
    }
    updateCartCount();
  }).fail(function() {
    console.error('minicart.js: updateMinicart error')
  })
}

const updateCartCount = function() {
  let cartCount = '[data-cart-count]';
  data.cart.item_count > 0 ? $(cartCount).show() : $(cartCount).hide() 
  $(cartCount).text(data.cart.item_count);
}

const eventHandlers = function() {
  $('[data-add-to-cart]').on('click', function(e) {
    e.preventDefault();
    addToCartHandler();
  });

  $(document).on('click', '[data-remove-from-cart]', removeFromCart);

  $('[data-minicart-toggle]').on('click', function(e) {
    e.preventDefault();
    $minicart.toggleClass('is-open');
  });

  $(document).on('cart.requestComplete', function (event, cart) {
    updateMinicart();
  });
  $(document).ready(function() {
    updateMinicart();
  });
}

const init = function() {
  if (tinybind) {
    tinybind.formatters.formatMoney = formatMoney;
    tinybind.formatters.showVariantTitle = function (title) {
      if (title.toLowerCase() == "default title") return false;
      return true;
    };
    tinybind.formatters.append = function(value, append) {
      return value+append;
    }
    tinybind.bind($minicart, data);
  } else {
    console.error("minicart.js: Tinybind template library is not connected");
    return false;
  }
  
}

export default function() {
  init();
  eventHandlers();
}
