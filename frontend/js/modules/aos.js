import $ from 'jquery';

const aos = function () {
    AOS.init();
}

export default aos;