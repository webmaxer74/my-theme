import slider from './slider';
import swatches from './swatches';
import goToProduct from './goToProduct';
import colorSwatches from './colorSwatches';

export default function() {
  slider();
  swatches();
  goToProduct();
  colorSwatches();
}