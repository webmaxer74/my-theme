import $ from 'jquery';
import { formatMoney } from '/tools/moneyFormats';

const $variantsSelector = $('[data-all-variants]');
const $productOption = $('[data-product-option]');

const updateProduct = function (evt) {
  const $variantSelected = $('option:selected', $variantsSelector);
  updatePrice($variantSelected);
  updateOptions($variantSelected);
  updateButton($variantSelected);
  updateLowStock($variantSelected);
  updateImages($variantSelected)
}

const updateImages = function($variantSelected) {
  const material = $variantSelected.data('option1');
  const color = $variantSelected.data('option2');
  $('[data-product-photo]').each(function(index, image) {
    const alt = $(image).data('product-photo');
    if (alt.includes(material) && alt.includes(color)) {
      $(image).show();
    } else {
      $(image).hide();
    }
  })
  let featured = $('[data-product-photo]').filter(':visible').eq(0).find('img').attr('src');
  if (!featured) featured = $variantSelected.data('variant-image');
  $('[data-product-featured-image]').attr('src', featured);
  $('[data-product-photo]').filter(':visible').eq(0).hide();
}

const updatePrice = function($variantSelected) {
  const $price = $('[data-price]');
  const $priceCompare = $('[data-price-compare]');
  const price = $variantSelected.data('variant-price');
  const comparePrice = $variantSelected.data('variant-compare-price');
  $price.text(formatMoney(price));
  comparePrice > 0 ? $priceCompare.text(formatMoney(comparePrice)).show() : $priceCompare.hide();
}

const updateButton = function($variantSelected) {
  const $addToCart = $('[data-add-to-cart]');
  const $etaMessage = $('[data-eta]');
  if ($variantSelected.data('available') == true) {
    if ($variantSelected.data('inventory-policy') == 'continue' && $variantSelected.data('inventory-quantity') <= 0) {
      $etaMessage.removeClass('hide');
      $addToCart.attr('disabled', false).text(window.theme.strings.reOrder);
    } else {
      $etaMessage.addClass('hide');
      $addToCart.attr('disabled', false).text(window.theme.strings.addToCart);
    }
  } else {
    $addToCart.attr('disabled', true).text(window.theme.strings.soldOut);
  }
}

const updateLowStock = function($variantSelected) {
  const $lowStock = $('[data-low-stock]');
  if ($variantSelected.data('inventory-quantity') == 1) {
    $lowStock.removeClass('hide');
  } else {
    $lowStock.addClass('hide');
  }
}

const updateOptions = function($variantSelected) {
  // Update option 1
  $('[data-product-option=1] option').each(function (index, option1) {
    let disabled = true;
    const optionVal = $(option1).val();
    $variantsSelector.find('option').each(function (index, variant) {
      if ($(variant).data('option1') == optionVal && $(variant).data('available') == true) {
        disabled = false;
      }
    });
    if (disabled) {
      $(option1).attr('disabled', true);
    }
  });

  // Update options 2 and 3
  const option1 = $variantSelected.data('option1');
  $variantsSelector.find('option').each(function (index, variant) {
    if ($(variant).data('option1') == option1) {
      const option2 = $(variant).data('option2');
      const $option2 = $(`[data-product-option=2] [value=${option2}]`);
      const option3 = $(variant).data('option3');
      const $option3 = $(`[data-product-option=3] [value=${option3}]`);
      if ($(variant).data('available') == false) {
        $option2.attr('disabled', true);
        $option3.attr('disabled', true);
      } else {
        $option2.attr('disabled', false);
        $option3.attr('disabled', false);
      }
    }
  });

  // Update option 2
  $('[data-product-option=2] option').each(function (index, option2) {
    let disabled = true;
    const option2Val = $(option2).val();
    const hasVariant = $variantsSelector.find(`[data-option1=${option1}][data-option2=${option2Val}]`);
    if (hasVariant && hasVariant.length) {
      $(option2).removeAttr('disabled');
    } else {
      $(option2).removeAttr('selected');
      $(option2).attr('disabled', true);
    }
  });
  $('[data-product-option=2]').val($variantSelected.data('option2'));
}

export default function() {
  // Variants changing (automatically triggers from options changing)
  $variantsSelector.on('change', function() {
    updateProduct();
  });

  // Options changing

  
  $productOption.change( function () {
    const option1 = $('[data-product-option=1] option:selected').length ? $('[data-product-option=1]').val() : '_BLANK_';
    const option2 = $('[data-product-option=2] option:selected').length ? $('[data-product-option=2]').val() : '_BLANK_';
    const option3 = $('[data-product-option=3] option:selected').length ? $('[data-product-option=3]').val() : '_BLANK_';
    console.log(option1, option2, option3);
    let value = $variantsSelector.find(`option[data-option1=${option1}][data-option2=${option2}][data-option3=${option3}]`).val();
    if (value === undefined) {
      value = $variantsSelector.find(`option[data-option1=${option1}]`).eq(0).val();
    }
    $variantsSelector.val(value).trigger('change');
    console.log('variantselector value', $variantsSelector.val());
  });

  updateProduct();

  $('[data-monogram-product]').on('change', function() {
    const $input = $('[data-monogram-input]');
    $input.val('').css('opacity', 0);
    if ($(this).is(':checked')) {
      $input.css('opacity', 1);
    }
  })
}