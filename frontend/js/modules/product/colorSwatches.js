import $ from 'jquery';


const updateImageAlt = function($variantSelected, $parent) {
  const alt = $variantSelected.data('alt');
  $parent.find('[data-product-image]').attr('alt', alt);
  $parent.find('[data-product-hover-image]').attr('alt', alt);
  $(window).trigger('updateCollectionImages');
}

const updateSwatches = function($variantSelected, $parent) {
  $parent.find('[data-color-swatch]').removeClass('is-active');
  $variantSelected.addClass('is-active');
}

const updateRefs = function($variantSelected, $parent) {
  const url = $variantSelected.data('variant-url');
  $parent.find('a').attr('href', url);
}


export default function() {
  $(document).on('click', '[data-color-swatch]', function() {
    const $variantSelected = $(this);
    const $parent = $(this).closest('[data-product-cell]')
    updateImageAlt($variantSelected, $parent);
    updateSwatches($variantSelected, $parent);
    updateRefs($variantSelected, $parent);
  })
}