import $ from 'jquery';

const animation = function () {
  const sections = $('.section, [data-animate-section]');
  $(document).scroll(function () {
    const scroll = $(this).scrollTop();
    sections.each(function (index, section) {
      const sectionTop = $(section).position().top - $(window).outerHeight()/1.5;
      if (scroll >= sectionTop) {
        $(section).addClass('section-in');
      }
    });
  }).scroll();
  $(document).ready(function () {
    $('body').addClass('body-in');
  });
}

export default animation;