import $ from 'jquery';

const init = function() {
  const api = 'https://freegeoip.live/json/';
  const locationCode = localStorage.getItem('locationCode');
  if (locationCode == undefined) {
    $.get(api, function (geo, status) {
      if (status == 'error') {
        console.error('Geo api '+ api + ' returned an error');
        return;
      }
      const code = geo.country_code.toLowerCase();
      console.log('Location detected from api:', code);
      localStorage.setItem('locationCode', code);
      updateCurrency(code);
    }).fail(function() {
      console.error('Geo api is missing', api);
    })
  } else {
    console.log('Location detected from localstorage:', locationCode);
  }
}

const updateCurrency = function(code) {
  let locationCurrency = 'EUR';
  if (code == 'gb') locationCurrency = 'GBP';
  if (code == 'us') locationCurrency = 'USD';
  const selectedCurrency = $('[data-currency-selector]').val();
  if (selectedCurrency != locationCurrency) {
    $('[data-currency-selector]').val(locationCurrency).trigger('change');
  }
}

export default function () {
  init();
}