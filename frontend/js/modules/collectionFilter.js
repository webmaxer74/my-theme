import $ from 'jquery';
import select2 from 'select2';
import slate from '../tools/images';

const filterVariantsByColors = function() {
  const selectedColors = window.globofilter.data.filters[1].values.filter(color => color.selected).map(color => color.value);
  if (selectedColors.length) {
    $('[data-product-variant-color]').each(function (index, variant) {
      const color = $(variant).data('product-variant-color');
      if (!selectedColors.includes(color)) {
        $(variant).remove();
      }
    });   
  }
}

const filterVariantsByMaterials = function() {
  const selectedMaterials = window.globofilter.data.filters[0].values.filter(material => material.selected).map(material => material.value);
  if (selectedMaterials.length) {
    $('[data-product-variant-material]').each(function (index, variant) {
      const material = $(variant).data('product-variant-material');
      if (!selectedMaterials.includes(material)) {
        $(variant).remove();
      }
    });   
  }
}

const sortByPrice = function() {
  const sortBy = $('#changeSortBy').val();
  if (sortBy == 'price-ascending' || sortBy == 'price-descending') {
    let $products = $("[data-product-variant-price]");
    $products.sort(function(a, b) {
      const aPrice = parseInt($(a).data("product-variant-price"));
      const bPrice = parseInt($(b).data("product-variant-price"));
      if (sortBy == 'price-ascending') return aPrice - bPrice;
      if (sortBy == 'price-descending') return bPrice - aPrice;
    });
    $("#gf-products").html($products);
  }
}


const updatePrice = function() {
  $('[data-product-variant-material]').each(async function (index, product) {
    const $price = $(product).find('[data-product-cell-price]');
    const $priceCompare = $(product).find('[data-product-cell-price-compare]');
    const url = $(product).find('a').attr('href');
    $.ajax({
      type: "GET",
      url: url,
      success: function (response) {
        const ownerDocument = document.implementation.createHTMLDocument('virtual');
        const price = $(response, ownerDocument).find('[data-price]').text();
        const priceCompare = $(response, ownerDocument).find('[data-price-compare]').text();
        $price.text(price).css('opacity', 1);
        $priceCompare.text(priceCompare).css('opacity', 1);
      }
    });
  });
}

const initSortByCustomSelect = function() {
  var formatState = function(data, container) {
    return `SORT BY ${data.text}`;
  }
  $('#changeSortBy').select2({
    templateSelection: formatState,
    minimumResultsForSearch: -1
  });
}

const updateImages = function() {
  $('[data-product-images]').each(function (index, variant) {
    let images = $(variant).data('product-images');
    const $image = $(variant).find('[data-product-image]');
    const $imageHover = $(variant).find('[data-product-hover-image]');
    const alt = $image.attr('alt');
    images = images.filter(image => image.alt == alt);
    if (images[0]) $image.attr('src', slate.Image.getSizedImageUrl(images[0].src, '700x'));
    if (images[1]) $imageHover.attr('src', slate.Image.getSizedImageUrl(images[1].src, '700x'));
  });
}

export default function() {
  $(window).on('globoFilterRenderCompleted', function(a, b, c) {
    filterVariantsByColors();
    filterVariantsByMaterials();
    sortByPrice();
    updatePrice();
    $(window).trigger('updateCollectionImages');
    initSortByCustomSelect();
  })
  $(window).on('updateCollectionImages', updateImages);
  updateImages();
}