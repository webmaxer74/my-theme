import $ from 'jquery';

const productVideo = function () {
    const $playButton = $('[data-product-video]');
    $($playButton).on( "click", function(e) {
        e.preventDefault();
        $(this).next('.product__video-wrapper').toggleClass('hide');
        $(this).next('.product__video-wrapper video').trigger('pause');
        $(this).text($(this).text() == 'Video' ? 'Close' : 'Video');
      });
}

export default productVideo;