export default function () {
  /******************
  COOKIE NOTICE
  ******************/
  var selectors = {
    popup: '[data-s-popup]',
    overlay: '[data-s-popup-overlay]'
  }
  var timeToShow = 100;
  // var timeToShow = 8000;
  if (getCookie('show_cookie_message') != 'no') {
    $(selectors.popup+', '+selectors.overlay).delay(timeToShow).queue(function () {
      $(this).addClass("show").dequeue();
    });
  }

  $(selectors.popup).find('.close-btn').click(function () {
    $(selectors.popup+', '+selectors.overlay).removeClass("show");
    // setCookie('show_cookie_message', 'no');
    return false;
  });

  // Cookie Settings (for Popup)
  function setCookie(cookie_name, value) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + (365 * 25));
    document.cookie = cookie_name + "=" + escape(value) + "; expires=" + exdate.toUTCString() + "; path=/";
  }

  function getCookie(cookie_name) {
    if (document.cookie.length > 0) {
      let cookie_start = document.cookie.indexOf(cookie_name + "=");
      if (cookie_start != -1) {
        cookie_start = cookie_start + cookie_name.length + 1;
        let cookie_end = document.cookie.indexOf(";", cookie_start);
        if (cookie_end == -1) {
          cookie_end = document.cookie.length;
        }
        return unescape(document.cookie.substring(cookie_start, cookie_end));
      }
    }
    return "";
  }

  function formError(id) {
    console.log(id);
    $(`#${id} #mce-success-response`).hide();
    $(`#${id} #mce-error-response`).show();
  }

  function formSuccess(id) {
    console.log(id);
    $(`#${id} #mce-error-response`).hide();
    $(`#${id} #mce-success-response`).show();
  }

  function register($form) {
    const id = $form.attr('id');
    var blackFridayPage = ".black-friday-form";

    jQuery.ajax({
      type: "GET",
      url: $form.attr('action'),
      data: $form.serialize(),
      cache: false,
      dataType: 'jsonp',
      contentType: "application/json; charset=utf-8",
      error: function (err) {
        console.error('Error sending form', err)
      },
      success: function (data) {
        if (data.result != "success") {
          console.log('error');
          formError(id);
        } else {
          console.log('success');
          if ($(`#${id}`).is(blackFridayPage)) {
            $(location).attr("href", '/pages/black-friday-confirmation');
          } else {
            formSuccess(id);
          }
        }
      }
    });
  }

  // waits for form to appear rather than appending straight to the form. Also helps if you have more than one type of form that you want to use this action on.
  const forms = ['#mc-embedded-subscribe-form-1', '#mc-embedded-subscribe-form-2', '#mc-embedded-subscribe-form-3', '.js-landing-forms'].join(',');

  $(forms).on('submit', function (event) {
    try {
      //define argument as the current form especially if you have more than one
      var $form = jQuery(this);
      // stop open of new tab
      event.preventDefault();
      // submit form via ajax
      register($form);
    } catch (error) {

    }
  });
}