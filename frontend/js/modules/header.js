import $ from 'jquery';

const stickyHeader = function() {
  const $header = $('[data-site-header]');
  let topBarHeight, headerHeight;
  const update = () => {
    headerHeight = ($header.hasClass('m-transparent')) ? 0 : $header.outerHeight();
    $('body').css('paddingTop', headerHeight+'px');
  }
  update();
  $(window).resize(update);
  $(document).scroll(function () {
    const scroll = $(this).scrollTop();
    if (scroll > headerHeight) {
      $header.css('transform', `translate(0, -${topBarHeight}px)`).addClass('is-scrolled');
    } else {
      $header.css('transform', `translate(0, 0)`).removeClass('is-scrolled');
    }
  })
}

const mobileHeader = function() {
  const $btn = $('[data-open-menu]');
  $btn.click(function(e) {
    e.preventDefault();
    $('body').toggleClass('is-menu-open');
  })
}

// const scrollTopNav = function() {
//   var stickyOffset = $("[data-site-header]").offset();
//   var $contentDivs = $(".shopify-section");
//   $(document).scroll(function() {
//       $contentDivs.each(function(k) {
//           var _thisOffset = $(this).offset();
//           var _actPosition = _thisOffset.top - $(window).scrollTop();
//           if (_actPosition < stickyOffset.top && _actPosition + $(this).height() > 0) {
//               $("[data-site-header]").removeClass("section-light section-dark").addClass($(this).hasClass("section-light") ? "section-light" : "section-dark");
//               return false;
//           }
//       });
//   });
// }

const header = () => {
  stickyHeader();
  mobileHeader();
  // scrollTopNav();
}

export default header