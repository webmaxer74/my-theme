export default function() {
  const update = () => {
    setTimeout(() => {
      if ($(window).width() <= 989) {
        $('body').css('paddingBottom', 0);
        $('[data-site-footer]').show();
        return false;
      }
      var height = $('[data-site-footer]').outerHeight();
      $('body').css({
        'paddingBottom': height
      });
      $('[data-site-footer]').show();
    }, 250);
  }
  update();
  $(window).resize(update)
}