import $ from 'jquery';

export default function() {
  const video = $('[data-video] video');
  const btn = $('[data-video-btn]');
  btn.click(function (e) {
    $(this).siblings('video').click();
  });
  video.each(function (index, videoEl) {
    $(videoEl).on('play seeking', function () {
      $(this).attr('controls', true);
      $(this).siblings('button').hide();
      $(this).closest('[data-video]').addClass('is-playing');
    })
    $(videoEl).on('seeked', function() {
      $(this)[0].play()
    })
    $(videoEl).on('pause', function () {
      $(this).removeAttr('controls');
      $(this).siblings('button').show();
      $(this).closest('[data-video]').removeClass('is-playing');
    })
  });
}